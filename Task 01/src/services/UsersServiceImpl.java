package services;

import dto.SignUpForm;
import models.User;
import repositories.UsersRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final Function<SignUpForm, User> toUserMapper;

    public UsersServiceImpl(UsersRepository usersRepository, Function<SignUpForm, User> toUserMapper) {
        this.usersRepository = usersRepository;
        this.toUserMapper = toUserMapper;
    }

    @Override
    public void signUp(SignUpForm signUpForm) {
        User user = toUserMapper.apply(signUpForm);
        boolean flag = false;
        for (var a : usersRepository.findAll()) {
            if (user.getEmail().equals(a.getEmail())) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            usersRepository.save(user);
        }
    }
}
