package services;

import dto.SignUpForm;

public interface UsersService {
    void signUp(SignUpForm signUpForm);
}
