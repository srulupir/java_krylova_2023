package app;

import dto.SignUpForm;
import mappers.Mappers;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryFilesImpl;
import services.UsersService;
import services.UsersServiceImpl;

import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Tatyana", "Krylova",
                "tatya@gmail.com", "qwerty001"));
        usersService.signUp(new SignUpForm("Polina", "Ryf",
                "polya@gmail.com", "qwerty002"));
        usersService.signUp(new SignUpForm("Elena", "Gus",
                "helen.gus@gmail.com", "qwerty003"));
        usersService.signUp(new SignUpForm("Alsu", "H",
                "uzbeki@gmail.com", "qwerty004"));


        List<User> users = usersRepository.findAll();
        System.out.println("Users:");
        for (var user : users) {
            System.out.println(user);
        }

        System.out.println("Found by id:");
        System.out.println(usersRepository.findById(users.get(2).getId()));

        User newUser = new User(users.get(0).getId(), "Rena", "Krylova", "qq@gmail.ru", "qwerty222");

        usersRepository.update(newUser);
        System.out.println("Update:");
        for (var user : usersRepository.findAll()) {
            System.out.println(user);
        }

        usersRepository.delete(users.get(1));
        System.out.println("delete:");
        for (var user : usersRepository.findAll()) {
            System.out.println(user);
        }

        usersRepository.deleteById(users.get(3).getId());
        System.out.println("Delete by id:");
        for (var user : usersRepository.findAll()) {
            System.out.println(user);
        }
    }
}
