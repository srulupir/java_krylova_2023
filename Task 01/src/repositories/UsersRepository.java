package repositories;

import models.User;

import java.util.UUID;

public interface UsersRepository extends CrudRepository<UUID, User> {
}
