package repositories;

import models.User;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;

import static com.sun.tools.attach.VirtualMachine.list;


public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;

    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(Paths.get(fileName));
            while (scanner.hasNext()) {
                String[] curInf = scanner.nextLine().split("\\|");
                for (var a : curInf) {
                    users.add(new User(UUID.fromString(curInf[0]), curInf[1], curInf[2], curInf[3], curInf[4]));
                    break;
                }
            }
        }
        catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }
    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());

        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        deleteById(entity.getId());
        save(entity);
    }

    @Override
    public void delete(User entity) {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(UUID aString) {
        List<User> list = findAll();
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
            for(var u : list) {
                if (!u.getId().equals(aString)){
                    String userAsString = userToString.apply(u);
                    bufferedWriter.write(userAsString);
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public User findById(UUID aLong) {
        User user = null;
        for(var b: findAll()){
           if(b.getId().equals(aLong)){
               user = b;
           }
        }
        return user;
    }
}
