--1
select c.model, c.speed, c.hd
from PC c
where c.price < 500;

--2
select distinct c.maker
from Product c
where c.type = 'Printer';


-- 3
select c.model, c.ram, c.screen
from Laptop c
where c.price > 1000;

--4
select c.code, c.model, c.color, c.type, c.price
from Printer c
where c.color = 'y';

--5
select c.model, c.speed, c.hd
from PC c
where ((c.CD = '12x' or c.CD = '24x') and c.price < 600);

--6
select distinct maker, speed
from Product inner join Laptop on
Product.model = Laptop.model where (laptop.hd >=10);

--7
select Product.model, PC.price
from Product inner join PC on
Product.model = PC.model
where maker = 'B'
union
select Product.model, Laptop.price
from Product inner join Laptop on
Product.model = Laptop.model
where maker = 'B'
union
select Product.model, Printer.price
from Product inner join Printer on
Product.model = Printer.model
where maker = 'B';

--8
select distinct maker from product
where type = 'PC'
except
select distinct maker from product
where type = 'Laptop';

--9
select distinct maker from Product
join PC on Product.model = PC.model
where PC.speed >= 450;

--10
select model, price from Printer
where price = (select max(price) from Printer);


--11
select avg(speed) from PC;


--12
select avg(speed) from Laptop where price > 1000;

--13
select avg(speed) from PC
left join Product on Product.model = PC.model
where product.maker = 'A';


--15
select hd from PC
group by hd
having count(hd) > 1;

--16
select distinct pc1.model, pc2.model, pc1.speed, pc1.ram
from pc as pc1, pc as pc2
where pc1.model > pc2.model and pc1.speed = pc2.speed and pc1.ram = pc2.ram;


--17
select distinct Product.type, Laptop.model, Laptop.speed
from Laptop, Product
where speed <
(select min(speed) from PC) and Product.type = 'Laptop';

--18
select distinct maker, price
from Product
join Printer on
Product.model = Printer.model
where price = (select min(price) from Printer where color = 'y')
and color = 'y';

--19
select maker, avg(screen) from
Product join Laptop on
Product.model = Laptop.model
group by maker;

--20
select maker, count(model) from Product
where type = 'PC'
group by maker
having count(model) >= 3;

--21
select maker, max(price) from Product
join PC on
Product.model = PC.model
group by maker;

--22
select speed, avg(price) from PC
where speed > 600
group by speed;

--23
select distinct maker
from Product p1 join PC p2 on p1.model = p2.model
where speed >= 750
  and maker in
      (select maker
       from Product p1 join Laptop p2 on p1.model = p2.model
       where speed >= 750);

--24
with mp as (
    select model, price from pc
    union
    select model, price from printer
    union
    select model, price from laptop
)
select model from mp where price = (select max(price) from mp);

--25
select distinct maker from Product
where model in
      (
          select model
          from PC
          where ram = (select min(ram) from PC
          )
            and speed =
                (select max(speed) from PC where ram = (select min(ram) from PC)))
  and
        maker in ( select maker from Product where type = 'Printer');

--31
select class, country
from classes
where bore >= 16;

--32
select country, cast(avg(bore*bore*bore/2) as numeric(38,2))
from
    (select country, name, bore
     from ships sh join classes c on sh.class=c.class
     union
     select country, ship, bore
     from outcomes o
    join classes c on o.ship = c.class) t1
    group by country;


--33
select ship
from Outcomes
where battle = 'North Atlantic' and result = 'sunk';

--34
select s.name from ships s
join classes c on s.class = c.class
where
s.launched >= 1922
and c.displacement > 35000
and type='bb';

--35
select model, type from product
where
model not like '%[^0-9]%' or model not like '%[^a-z]%';


--38
select distinct country from Classes
where type = 'bb' and country in
    (
    select country from Classes
    where type = 'bc'
    );

--42
select ship, battle
from Outcomes
where result = 'sunk';

--44
select *
from
    (
        select name
        from Ships
        union
        select ship
        from Outcomes
    )a where name like 'R%';

--45
select * from
    (
        select name
        from Ships
        union
        select ship
        from Outcomes
    )
        a where name like '% % %';

--46
select ship, displacement, numGuns
from Outcomes a
left join Ships c on a.ship = c.name
left join Classes b on a.ship = b.class or c.class = b.class
where battle = 'Guadalcanal';


--48
select class
from Classes t1 left join Outcomes t2
on t1.class=t2.ship where result='sunk'
union
select class
from Ships left join Outcomes on Ships.name = Outcomes.ship
where result = 'sunk';

--49
select name
from ships s join classes c on s.class = c.class
where bore = 16
union
select ship
from outcomes o join classes c on o.ship = c.class
where bore = 16;

--50
select distinct battle from outcomes o
join ships s on o.ship = s.name
join classes c on c.class = s.class
where c.class = 'Kongo';

--52
select s.name
from ships s
join classes c on c.class = s.class
where country = 'Japan'
  and type='bb' and (numguns >= 9 or numguns is null)
  and (bore < 19 or bore is null)
  and (displacement <= 65000 or displacement is null);


--53
select
cast(round(avg(cast(numGuns as numeric(4,2))),2) as numeric(4,2))
from classes
where type='bb';

--55
select
c.class, min(launched)
from classes c
full join ships s on c.class = s.class
group by c.class;














