insert into client(first_name, last_name, age) values ('Ivan', 'Ivanov', 20);
insert into client(first_name, last_name, age) values ('Jong-Un', 'Kim', 39);
insert into client(first_name, last_name, age) values ('Il-Sung', 'Kim', 82);
insert into client(first_name, last_name, age) values ('Jong-Il', 'Kim', 70);

insert into driver(first_name, last_name, rating, phone_number)
values ('Djamshoot','Djamshootbekov', 4, '89379532222'),
       ('Ashkudi', 'Ashkudinov', 5, '89003261452'),
       ('Vasya', 'Vaskin', 3 , '89276542856');

insert into car(model, number, classs, year_of_release)
values ('RENAULT LOGAN', 'B123OA21', 'econom' , 2005),
       ('Toyota Camry 3.5', 'O854BB21' , 'bisness', 2010),
       ('Hyundai Solaris', 'E336XO21' , 'econom', 2011);

insert into ordering(order_date, order_time)
values ('2022-06-15', '15:16'),
       ('2022-07-01', '10:12'),
       ('2022-07-02', '17:20');

update driver set driving_experience = 4 where id = 1;
update driver set driving_experience = 8 where id = 2;
update driver set driving_experience = 2 where id = 3;

update ordering set driver_id = 1 where id = 1;
update ordering set driver_id = 2 where id = 2;
update ordering set driver_id = 3 where id = 3;

update ordering set client_id = 1 where id = 1;
update ordering set client_id = 2 where id = 3;
update ordering set client_id = 3 where id = 2;

update client set phone_number = '89003330707' where id = 1;
update client set phone_number = '89003845707' where id = 2;
update client set phone_number = '89075420707' where id = 3;
update client set phone_number = '89275830707' where id = 4;