drop table if exists ordering;
drop table  if exists client;
drop table if exists driver;
drop table if exists car;

create table client(
        id bigserial primary key,
        first_name char(20),
        last_name char(20),
        age integer check (age > 16 and age < 120)
);

create table driver(
        id bigserial primary key,
        first_name char(20),
        last_name char(20),
        rating integer check (rating > 0 and rating < 6),
        phone_number char(11)
);

create table car(
        model char(30),
        number char(8),
        classs char(10) check (classs = 'econom' or classs = 'bisness'),
        year_of_release integer check (year_of_release > 1885 and year_of_release < 2023)
);

create table ordering(
        id bigserial primary key,
        order_date date,
        order_time time
);

alter table client add phone_number char(11) not null default '';

alter table driver add driving_experience bigint;

alter table ordering add driver_id bigint;
alter table ordering add client_id bigint;
alter table ordering add foreign key(driver_id) references driver(id);
alter table ordering add foreign key(client_id) references client(id);
