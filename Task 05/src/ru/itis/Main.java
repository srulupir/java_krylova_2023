package ru.itis;

import ru.itis.dto.StudentSignUp;
import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("Task 05/resources/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        System.out.println(studentsRepository.findAll());

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);

        System.out.println("finding All By Age Greater Than Order By Id Desc");
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(25));

        System.out.println("updating:");
        System.out.println(studentsRepository.findAll());
        Student student1 = studentsRepository.findById(1L).get();
        student1.setFirstName("Mark");
        student1.setLastName("aboba");

        studentsRepository.update(student1);
        System.out.println(studentsRepository.findAll());

        System.out.println("deleting:");
        System.out.println(studentsRepository.findAll());
        studentsRepository.delete(2L);
        System.out.println(studentsRepository.findAll());
    }
}
